﻿repülő: (plane.blend)
0. Kitöröltem az alapból megjelenő kocka testet.
1. Első lépésként egy Cylinder-t szurtam be az üres rajzterületre.
2. Edit Mode-ban rotáltam a test fekvését a szerkesztéshez, méretet állítottam neki.
3. A repülőgép alapjait CTRL+R parancs, és az egér (+ SHIFT) segítségével beállítottam (orr, pilótafülke, törzs)
4. Ezt követően levágtam a repülő felét, majd a Modifier-ekből a tükrözést kiválasztottam X tengelyre, hogy a szerkesztés mindkét oldalon egyforma legyen.
5. Utolsó három fő rész a szárny, a farok és a hajtómű következett, melyeknél CTRL+TAB-bal kiválasztottam a FACE-t, hogy kitudjam jelölni a felületet B gomb segítségével, és az E gombbal, illetve rotációval megformáztam több lépésben, amíg a kívánt alakot nem kaptam.
Megjegyzés:
-A műveletek során a nézet váltáshoz SHIF+egér görgőt használtam.
-A repülő az A-10A repülőgépről lett mintázva többé kevésbé.

hangár: (hangar.blend)
0. Kitöröltem az alapból megjelenő kocka testet.
1. Első lépésként egy Cylinder-t szurtam be az üres rajzterületre.
2. Edit mode-ban kézzel leméreteztem, illetve megfelelően rotáltam a későbbi exportáláshoz.
3. B + egér segítségével kijelöltem, majd kitöröltem a forma alsó felét.
4. CTRL+R parancssal és egérrel elkészítettem a hangár belsejét. (beljebb toltam az elejét)
5. További külalak formázást végeztem E-vel, illetve egyes részek kitörlésével.
Megjegyzés:
-A műveletek során a nézet váltáshoz SHIF+egér görgőt használtam.

hőlégballon: (ballon.blend)
0. Kitöröltem az alapból megjelenő kocka testet.
1. Gömb formát beszurtam az üres rajzterületre.
2. Edit Mode-ban méreteztem a szerkesztéshez.
3. CTRL+R, E és a méretezés segítségével egyedire formáztam a gömböt, ami után megkaptam a ballon alakját.
	3.1 A ballon alsóbb részeit összehúztam.
	3.2 CTRL+R segítségével a legalsó részt széthúztam közel ballonkosár alakra.
Megjegyzés:
-A műveletek során a nézet váltáshoz SHIF+egér görgőt használtam.

épület: (textureblendEpulet.blend)
0. Meghagytam a kezdetben megjelenő kocka testet.
1. A kovkatestet tégla testté formáztam, hogy hasonlítson egy épületre.
2. letöltöttem 3 képet, majd egy képként összeszerkesztve használtam fel a blenderes formához texturaként.
3. Az órán használt weblapon található képekkel illusztrált lépéssorozatot végrehajtva textúrát adtam a téglatesthez.
	3.1 http://www.inf.u-szeged.hu/~tanacs/threejs/blender_anyaghoz_rendelt_textra.html
	3.2 http://www.inf.u-szeged.hu/~tanacs/threejs/kocka_laponknti_textrzsa.html
Megjegyzés:
-A műveletek során a nézet váltáshoz SHIF+egér görgőt használtam.